<?php
namespace coreTests;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends \Codeception\Actor
{
    use \coreTests\_generated\AcceptanceTesterActions;

    public function fillCkEditorById($element_id, $content) {
        $this->fillRteEditor(
            \Facebook\WebDriver\WebDriverBy::cssSelector(
                '#cke_' . $element_id . ' .cke_wysiwyg_frame'
            ),
            $content
        );
    }

    public function fillCkEditorByName($element_name, $content) {
        $this->fillRteEditor(
            \Facebook\WebDriver\WebDriverBy::cssSelector(
                'textarea[name="' . $element_name . '"] + .cke .cke_wysiwyg_frame'
            ),
            $content
        );
    }

    public function fillCkEditorByClass($element_class, $content) {
        $this->fillRteEditor(
            \Facebook\WebDriver\WebDriverBy::cssSelector(
                'textarea[class="cke_wysiwyg_frame ' . $element_class . '"]'
            ),
            $content
        );
    }

    public function fillTinyMceEditorById($id, $content) {
        $this->fillTinyMceEditor('id', $id, $content);
    }

    public function fillTinyMceEditorByName($name, $content) {
        $this->fillTinyMceEditor('name', $name, $content);
    }

    private function fillTinyMceEditor($attribute, $value, $content) {
        $this->fillRteEditor(
            \Facebook\WebDriver\WebDriverBy::xpath(
                '//textarea[@' . $attribute . '=\'' . $value . '\']/../div[contains(@class, \'mce-tinymce\')]//iframe'
            ),
            $content
        );
    }

    private function fillRteEditor($selector, $content) {
        $this->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver)
            use ($selector, $content) {
                $webDriver->switchTo()->frame(
                    $webDriver->findElement($selector)
                );

                $webDriver->executeScript(
                    'arguments[0].innerHTML = "' . addslashes($content) . '"',
                    [$webDriver->findElement(\Facebook\WebDriver\WebDriverBy::tagName('body'))]
                );

                $webDriver->switchTo()->defaultContent();
            });
    }

    public function closeBrowser() {
        $this->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
                return $webDriver->close();
            });
    }

    public function quitBrowser() {
        $this->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
                return $webDriver->quit();
            });
    }

    public function getCurrentUrl() {
        return $this->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
                return $webDriver->getCurrentURL();
            }
        );
    }

    public function grabPageSource() {
        return $this->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
                return $webDriver->getPageSource();
            });
    }

    public function grabCookies() {
        return $this->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
                return $webDriver->manage()->getCookies();
            });
    }

    public function deleteCookies() {
        $this->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webDriver) {
                return $webDriver->manage()->deleteAllCookies();
            });
    }

    public function autoLoginID2Link() {
        $link = "https://id2.action-media.ru/fl";
        $appSecret = "1749794991";
        $sigparams = [
            "user"      => "4544952",
            "app"       => "62",
            "rand"      => (string)(time() * 10000000),
            "sign"      => "",
            "returnurl" => "https://www.zdrav.ru/",
        ];
        $sigparams["sign"] = md5(strtolower($sigparams["user"].$sigparams["app"].$sigparams["rand"].$sigparams["returnurl"].$appSecret));
        $link .= "?" . http_build_query($sigparams);

        return $link;
    }

    /**
     * Define custom actions here
     */
}
